STUDENT **Maha Asfour** (1181475) - P2.1
===============================
**integration of all the tools and technologies used during P1**

-The Analysis of the Problem:

* The overall goal is to simulate an integrated "devops" scenario.

- implement a pipeline for the CI of the TODD application.(using Jenkins)
- This pipeline should also include the CD of the application by publishing it(using Nexus) and eventually deploying it.(using Ansible)
- A small local network should be simulated(using Vagrant) (that includes the hosts where we will bedeploying TODD).
- The hosts and services should be monitored.(using Nagios)

* The network must include two hosts:

1. One with the TODD application.
2. One with Nagios and the TODD application. 

* Monitoring:

1. Nagios should monitor TODD (in the two hosts),and an event handler to try to automatically restar TODD.
2. Should use JMX to monitor TODD.


===============================

-The Design of Solution:

1. Fork the following repository (https://bitbucket.org/mei-isep/todd/src/master/) to work on it.
2. Replace line 8 in (Jenkinsfile) in our new repository with:

        git credentialsId: 'bitbucket-credential', url: 'https://bitbucket.org/Maha_Asfour/todd/' 

===============

**Implementation App & Publishing**
	
* Jenkins: The leading open source automation server, Jenkins provides hundreds of plugins to support building, deploying and automating any project .

3. Install Jenkins, and replace the httpPort=8080 (jenkins.xml) file with httpPort=9999 .
4. Run Jenkins with command line:
        
		java -jar jenkins.war

5. Open http://localhost:9999 to access Jenkins .
6. From Jenkins page New item > pipeline .(the name of pipeline:todd)
7. todd pipeline > configure > pipeline > choose: pipeline script from SCM > choose: git > Repository URL: https://bitbucket.org/Maha_Asfour/todd/ > add Credentials with ID: bitbucket-credential > save .

* Nexus-Repository: Nexus Repository OSS is an open source repository that supports many artifact formats, including Docker, Java™, and npm. With the Nexus tool integration, pipelines in your toolchain can publish and retrieve versioned apps and their dependencies by using central repositories that are accessible from other environments.

8. Download Nexus-Repository .
9. To start the repository manager from application directory in the bin folder on a Unix-like platform:

        ./nexus run
		
10. Access the user interface at http://localhost:8081/ , log in with username: admin , password: admin123 .

===============

**Simulate network & Deployment App**

* Vagrant: is an open-source software product for building and maintaining portable virtual software development environments,e.g. for VirtualBox, KVM, Hyper-V, Docker containers, VMware, and AWS.It tries to simplify software configuration management of virtualizations in order to increase development productivity. Vagrant is written in the Ruby language.

* vagrantfile: The main function of the Vagrantfile is to describe the virtual machines required for a project as well as how to configure and provision these machines.

11. add Vagrantfile to our repository,including:

        Vagrant.configure("2") do |config|
        config.vm.box = "envimation/ubuntu-xenial"
        config.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        end

        config.vm.provision "shell", inline: <<-SHELL
        sudo apt-get update -y
        sudo apt-get install iputils-ping -y
        sudo apt-get install python3 --yes
        SHELL

        config.vm.define "host1" do |host1|
        host1.vm.box = "envimation/ubuntu-xenial"
        host1.vm.hostname = "host1"
        host1.vm.network "private_network", ip: "192.168.33.11"

        host1.vm.network "forwarded_port", guest: 80, host: 8080
        end

        config.vm.define "host2" do |host2|
        host2.vm.box = "envimation/ubuntu-xenial"
        host2.vm.hostname = "host2"
        host2.vm.network "private_network", ip: "192.168.33.12"
        end

        config.vm.define "ansible" do |ansible|
        ansible.vm.box = "envimation/ubuntu-xenial"
        ansible.vm.hostname = "ansible"
        ansible.vm.network "private_network", ip: "192.168.33.10"

        ansible.vm.synced_folder ".", "/vagrant", mount_options: ["dmode=755,fmode=600"]

        ansible.vm.provision "shell", inline: <<-SHELL
        sudo apt-get install -y --no-install-recommends apt-utils
        sudo apt-get install software-properties-common --yes
        sudo apt-add-repository --yes --u ppa:ansible/ansible
        sudo apt-get install ansible --yes
        SHELL
        end
        end
	  
12. Add stage to Jenkinsfile to simulate the network:
 
        stage('vagrant-up') {
          steps {
              echo 'simulate network...'
			  sh 'vagrant up'
            }
        }
		
13. To configure Ansible,add hosts file to Repository,including:

        [all]
        host1 private_network_address=192.168.33.11 ansible_ssh_host=127.0.0.1 ansible_ssh_port=2222 ansible_ssh_private_key_file=.vagrant/machines/host1/virtualbox/private_key ansible_python_interpreter=/usr/bin/python3
        host2 private_network_address=192.168.33.12 ansible_ssh_host=127.0.0.1 ansible_ssh_port=2200 ansible_ssh_private_key_file=.vagrant/machines/host2/virtualbox/private_key ansible_python_interpreter=/usr/bin/python3
		 
14. Add ansible.cfg,that can be created alongside the playbooks,this file is used to setup some ansible defaults:

        [defaults]
        inventory = hosts
        remote_user = vagrant 

15. Add ansible playbook for nagios to provision host1 (nagios.yml):

        ---
        - name: Provision nagios
        hosts: host1
        become: yes
        tasks:
        - name: update apt cache
        apt: update_cache=yes
        - name: Set timezone to Europe/Lisbon
        timezone:
        name: Europe/Lisbon
        - name: Ensure Nagios requirements
        apt:
        name: ['autoconf', 'gcc', 'libc6', 'make', 'wget', 'unzip', 'apache2', 'php', 'libapache2-mod-php7.0', 'libgd2-xpm-dev', 'python3-passlib']
        - name: Download Nagios
        unarchive:
        src: https://github.com/NagiosEnterprises/nagioscore/archive/nagios-4.4.3.tar.gz
        copy: no
        dest: /home/vagrant/
        creates: /home/vagrant/nagioscore-nagios-4.4.3
        - name: Run ./configure for Nagios
        shell: './configure --with-httpd-conf=/etc/apache2/sites-enabled'
        args:
        chdir: /home/vagrant/nagioscore-nagios-4.4.3
        - name: Compile Nagios
        make:
        chdir: /home/vagrant/nagioscore-nagios-4.4.3
        target: all
        - name: Make install-groups-users
        make:
        chdir: /home/vagrant/nagioscore-nagios-4.4.3
        target: install-groups-users
        - name: Create Nagios user and group
        user:
        name: nagios
        group: nagios
        - name: Add www-data to nagios group
        user:
        name: nagios
        group: www-data
        append: yes
        - name: Make install
        make:
        chdir: /home/vagrant/nagioscore-nagios-4.4.3
        target: install
        - name: Intall install-daemoninit
        make:
        chdir: /home/vagrant/nagioscore-nagios-4.4.3
        target: install-daemoninit
        - name: Intall install-commandmode
        make:
        chdir: /home/vagrant/nagioscore-nagios-4.4.3
        target: install-commandmode
        - name: Intall install-config
        make:
        chdir: /home/vagrant/nagioscore-nagios-4.4.3
        target: install-config
        - name: Intall install-webconf
        make:
        chdir: /home/vagrant/nagioscore-nagios-4.4.3
        target: install-webconf
        - name: Install apache config files
        make:
        chdir: /home/vagrant/nagioscore-nagios-4.4.3
        target: install-webconf
        - name: Configure Apache
        shell: "{{ item }}"
        args:
        chdir: /home/vagrant/nagioscore-nagios-4.4.3
        with_items:
        - 'a2enmod rewrite'
        - 'a2enmod cgi'
        - name: Create nagiosadmin
        htpasswd:
        path: /usr/local/nagios/etc/htpasswd.users
        name: nagiosadmin
        password: nagios
        - name: Restart apache2
        systemd:
        name: apache2
        state: reloaded
        - name: Start Nagios
        systemd:
        name: nagios
        state: started
        enabled: yes
        - name: Ensure Nagios plugins requirements
        apt:
        name: ['autoconf', 'automake', 'autopoint', 'gcc', 'libc6', 'libmcrypt-dev', 'make', 'libssl-dev', 'wget', 'bc', 'gawk', 'dc', 'build-essential', 'snmp', 'libnet-snmp-perl', 'gettext']
        - name: Download Nagios plugins
        unarchive:
        src: https://github.com/nagios-plugins/nagios-plugins/archive/release-2.2.1.tar.gz
        copy: no
        dest: /home/vagrant/
        creates: /home/vagrant/nagios-plugins-release-2.2.1
        - name: Run ./tools/setup for Nagios plugins
        shell: './tools/setup'
        args:
        chdir: /home/vagrant/nagios-plugins-release-2.2.1
        - name: Run ./configure for Nagios plugins
        shell: './configure'
        args:
        chdir: /home/vagrant/nagios-plugins-release-2.2.1
        - name: Make for Nagios plugins
        shell: 'make'
        args:
        chdir: /home/vagrant/nagios-plugins-release-2.2.1
        - name: Make install for Nagios plugins
        shell: 'make install'
        args:
        chdir: /home/vagrant/nagios-plugins-release-2.2.1
        - name: Install Nagios NRPE plugin
        apt:
        name: nagios-nrpe-plugin
        - name: Recursively change ownership of /usr/local/nagios/var/rw/
        file:
        path: /usr/local/nagios/var/rw/
        state: directory
        recurse: yes
        owner: nagios
        group: www-data

16. Add ansible playbook to configure nagios in host1 (nagios-cfg.yml):

        ---
        - name: Configure nagios
        hosts: host1
        become: yes
        tasks:
        - name: Copy nagios.cfg file to server
        copy:
        src: ./nagios-cfg/nagios.cfg
        dest: /usr/local/nagios/etc/nagios.cfg
        - name: Copy otherhosts.cfg file to server
        copy:
        src: ./nagios-cfg/otherhosts.cfg
        dest: /usr/local/nagios/etc/objects/otherhosts.cfg
        - name: Copy commands.cfg file to server
        copy:
        src: ./nagios-cfg/commands.cfg
        dest: /usr/local/nagios/etc/objects/commands.cfg
        - name: copy eventhandlers folder
        copy:
        src: ./nagios-cfg/eventhandlers
        dest: /usr/local/nagios/libexec/eventhandlers
        - name: Restart Nagios
        systemd:
        name: nagios
        state: reloaded

17. Add ansible playbook for TODD to provision host1 (todd.yml):

        ---
        - name: Configure TODD application
        hosts: host1
        # the tasks of the playbook will be executed as "sudo"
        become: yes
        tasks:
        - name: update apt cache
        apt: update_cache=yes
        - name: install jdk
        apt: name=openjdk-8-jdk-headless state=present
        - name: Copy todd.jar file to server
        copy:
        src: ./todd/todd.jar
        dest: /home/vagrant/
        - name: Copy checkjmx file to server
        copy:
        src: ./todd/eventhandlers/check_jmx
        dest: /usr/lib/nagios/plugins
        owner: root
        mode: 0777
        - name: Copy jmxquery file to server
        copy:
        src: ./todd/eventhandlers/jmxquery.jar
        dest: /usr/lib/nagios/plugins
        mode: 0777
        - name: Copy eventhandlers file to server
        copy:
        src: "{{ item }}"
        dest: /usr/lib/nagios/plugins/
        owner: root
        mode: 0777
        with_fileglob:
        - /todd/eventhandlers/*
        - name: create todd.service file
        template:
        src: ./todd/todd.service.j2
        dest: /etc/systemd/system/todd.service
        - name: Start TODD
        systemd:
        name: todd
        state: started
        enabled: yes
        
18. Add ansible playbook for TODD to provision host2 (todd2.yml):

        ---
        - name: Configure TODD application
        hosts: host2
        # the tasks of the playbook will be executed as "sudo"
        become: yes
        tasks:
        - name: update apt cache
        apt: update_cache=yes
        - name: install jdk
        apt: name=openjdk-8-jdk-headless state=present
        - name: Copy todd.jar file to server
        copy:
        src: ./todd/todd.jar
        dest: /home/vagrant/
        - name: create todd.service file
        template:
        src: ./todd/todd.service.j2
        dest: /etc/systemd/system/todd.service
        - name: Start TODD
        systemd:
        name: todd
        state: started
        enabled: yes

19. Add ansible playbook for nrpe to provision host1 and host2 (nrpe.yml):

        ---
        - name: Configure nrpe
        hosts: host1,host2
        become: yes
        tasks:
        - name: update apt cache
        apt: update_cache=yes
        - name: configure and Install nrpe & Nagios plugins 
        apt:
        name: ['nagios-nrpe-server', 'nagios-plugins', 'nagios-plugins-basic', 'nagios-plugins-standard']
        - name: Copy nrpe configuration file to the apropriate directory
        copy:
        src: ./nrpe/nrpe.cfg
        dest: /etc/nagios/nrpe.cfg
        - name: Copy check file to server
        copy:
        src: ./todd/eventhandlers/check_jmx
        dest: /usr/lib/nagios/plugins
        owner: root
        mode: 0777
        - name: Copy jmxquery file to server
        copy:
        src: ./todd/eventhandlers/jmxquery.jar
        dest: /usr/lib/nagios/plugins
        mode: 0777
        - name: Restart nrpe service
        shell: /etc/init.d/nagios-nrpe-server restart
        - name: restart todd
        copy:
        src: ./todd/restart_todd.sh
        dest: /usr/lib/nagios/plugins
		
19. After adding these playbooks to our Repository,now we can add stage to To test if Ansible could connect to the virtual machines in Jenkinsfile:

        stage('test connection with ansible') {  
           steps {    
              dir('dev/ansible')
              { 
               sh 'ansible all -m ping -i hosts' 
            }
           }
        }
			
20. Add stage in Jenkinsfile to ask user confirmation before Ansible start deploying todd:
			
        stage('deploy TODD') {
          steps {
              echo 'Invoking playbooks...'
              input 'are you sure you want to deploy todd application?'
              ansiblePlaybook (credentialsId: 'private_key', playbook: 'nagios.yml')
              ansiblePlaybook (credentialsId: 'private_key', playbook: 'nagios-cfg.yml')
              ansiblePlaybook (credentialsId: 'private_key', playbook: 'todd.yml')
              ansiblePlaybook (credentialsId: 'private_key', playbook: 'todd2.yml')
              ansiblePlaybook (credentialsId: 'private_key', playbook: 'nrpe.yml')
            }
        }
		
21. Now,we can build todd pipeline in Jenkins,where the Jenkinsfile will execute the implementation of TODD application, publishing the successful builds of the application in Nexus artifact repository,simulate the network using Vagrant,and deploy todd application using ansible.


